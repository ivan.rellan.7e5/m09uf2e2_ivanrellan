﻿using System;
using System.Threading;

namespace M09UF2E2_IvanRellan
{
    class Program
    {
        static void Main()
        {
            //ThreadDefinitions();
            NeveraStartUp();
        }

        static void ThreadDefinitions()
        {
            Thread t1 = new Thread(() => {
                CatIntroduction();
            });

            Thread t2 = new Thread(() => {
                t1.Join();
                QuijoteIntroduction();
            });

            Thread t3 = new Thread(() => {
                t2.Join();
                InTheWest();
            });

            t1.Start();
            t2.Start();
            t3.Start();
        }

        static void CatIntroduction()
        {
            string[] words = { "Una", "vegada", "hi", "havia", "un", "gat" };
            SentenceGenerator(words, 500);
        }

        static void QuijoteIntroduction()
        {
            string[] words = { "En", "un", "lugar", "de", "la", "mancha" };
            SentenceGenerator(words, 1000);
        }

        static void InTheWest()
        {
            string[] words = { "Once", "upon", "a", "time", "in", "the", "west" };
            SentenceGenerator(words, 1500);
        }

        static void SentenceGenerator(string[] words, int delay)
        {
            foreach(string word in words)
            {
                Console.Write(word + " ");
                Thread.Sleep(delay);
            }

            Console.WriteLine("");
        }

        static void NeveraStartUp()
        {
            Nevera nevera = new Nevera(6, 9);
            NeveraThreads(nevera);
        }

        static void NeveraThreads(Nevera nevera)
        {
            Thread t1 = new Thread(() =>
            {
                nevera.AddBeers("Anitta");
            });
            Thread t2 = new Thread(() =>
            {
                t1.Join();
                nevera.DrinkBeers("Bad Bunny");
            });
            Thread t3 = new Thread(() =>
            {
                t2.Join();
                nevera.DrinkBeers("Lil Nas X");
            });
            Thread t4 = new Thread(() =>
            {
                t3.Join();
                nevera.DrinkBeers("Manuel Turizo");
            });

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();
        }
    }

    class Nevera
    {
        int Beers;
        int MaxBeers;

        public Nevera(int beers, int maxBeers)
        {
            Beers = beers;
            MaxBeers = maxBeers;
        }

        public void AddBeers(string name)
        {
            int AddedBeers = new Random().Next(0, 7);

            Console.Write(name + " adds " + AddedBeers.ToString() + " beers. ");

            if (Beers + AddedBeers <= MaxBeers)
            {
                Beers += AddedBeers;
            }
            else
            {
                Beers = MaxBeers;
                Console.Write("Not enough space. Only " + (MaxBeers - AddedBeers).ToString() + " added. ");
            }

            Console.WriteLine();
        }

        public void DrinkBeers(string name)
        {
            int SubstractedBeers = new Random().Next(0, 7);

            Console.Write(name + " drinks " + SubstractedBeers.ToString() + " beers. ");

            if (Beers - SubstractedBeers >= 0)
            {
                Beers -= SubstractedBeers;
            }
            else
            {
                Console.Write("Not enough beers. Only drank " + Beers.ToString());
                Beers = 0;
            }

            Console.WriteLine();
        }
    }
}
